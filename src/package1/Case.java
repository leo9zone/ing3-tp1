package package1;

public interface Case {
	public int getPositionX()
	; // retourne la position en X de la case
	public int getPositionY()
	; // retourne la position en Y de la case
	public boolean canMoveToCase()
	; //indique s’il est possible ou non d’aller dans la case
}